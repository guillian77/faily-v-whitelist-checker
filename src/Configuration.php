<?php


namespace App;


class Configuration
{
    public static function url()
    {
        return "http://panel.failyv.com/whitelist/index.php";
    }

    public static function mails()
    {
        return [
            "mail1@mail.com",
            "mail2@mail.com"
        ];
    }
}