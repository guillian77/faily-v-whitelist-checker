<?php


namespace App;


class Commands
{
    private $args;

    public function __construct()
    {
        $this->args = $_SERVER['argv'];

        if (!empty($this->args[1])) {
            if (method_exists($this, $this->args[1])) {
                call_user_func([$this, $this->args[1]]);
            } else {
                echo $this->args[1]." command does not exist!";
            }

            return;
        }

        echo "Specify a command in this list:\n";
        echo "> whitelistStatus\n";
    }

    private function whitelistStatus()
    {
        $status = Whitelist::check();

        $message = $this->formatMessage();

        if (!empty($this->args[2]) && mb_strpos($this->args[2], "--force") === 0)
        {
            (substr($this->args[2], 8) == "true") ? $status = true : $status = false;
            $message = $this->formatMessage(true);
        }

        if (!$status) {
            echo "Whitelist is currently close.";
            return false;
        }

        Mail::send(Configuration::mails(), "Whitelist Faily V", $message);
    }

    /**
     * Format message to send.
     *
     * @param bool $testing
     * @return string
     */
    private function formatMessage(bool $testing = false)
    {
        $message = "";

        if ($testing) {
            $message .= '<h1>Ceci est un test: à ignorer.</h1>';
        }

        $message .= '<p>L\'inscription à la whitelist est ouverte !</p>';
        $message .= '<p><a href="'.Configuration::url().'">Je m\'inscris !</a></p>';
        return $message;
    }
}