<?php


namespace App;


class Whitelist
{
    public static function get()
    {
        return file_get_contents(Configuration::url());
    }

    public static function check()
    {
        $stream = self::get();
        return (mb_strpos($stream, "complète")) ? false : true ;
    }
}