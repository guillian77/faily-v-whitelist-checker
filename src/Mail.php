<?php


namespace App;


class Mail
{
    /**
     * Send to mail list.
     *
     * @param array $targets Emails list.
     * @param string $subject Email title.
     * @param string $message Email content
     */
    public static function send(array $targets, string $subject, string $message)
    {
        foreach ($targets as $target) {
            mail($target, $subject, $message, 'Content-type: text/html; charset=iso-8859-1');
        }
    }
}