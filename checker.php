<?php
namespace App;

spl_autoload_register(function ($fullname) {
    $exploded = explode("\\", $fullname);
    $classname = $exploded[count($exploded)-1];
    $namespace = trim( str_replace($classname, "", $fullname), "\\" );

    require_once 'src/'.$classname.'.php';
});

new Commands();
