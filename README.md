# README #

Check if Faily V whitelist inscription is open or not. An email will be sent to suscriber list.

This have to be called with PHP CLI and a cron job.

* [Faily V Whitelist](http://panel.failyv.com/whitelist/index.php)

### Get started ###

```SH
# Run checker from command line.
php checker.php whitelistStatus

# Test checker.
php checker.php whitelistStatus --force=true

# Run checker from crontab every days at 10:00 am.
* 10 * * * php /var/www/faily-v-whitelist-checker/checker.php whitelistStatus >/dev/null 2>&1
```